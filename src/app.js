'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send 
app.get('/', (req, res) => { 
    var balance = 1000;
    if(!validateNumber(req.query.amount)){
        res.status(400).end('Insufficient funds. Your balance: ' + balance);
    }
    var { balance, transfered } = transfer(balance, req.query.amount);
    if (balance !== undefined || transfered !== undefined) { 
        res.status(200).end('Successfully transfered: ' + transfered + '. Your balance: ' + balance);
    } else {
        res.status(400).end('Insufficient funds. Your balance: ' + balance);
    }
});

// Transfer amount service
var transfer = (balance, amount) => {
    var transfered = 0;
    const _amount = +amount;
    if (_amount <= balance) {
        balance = balance - _amount;
        transfered = transfered + Number(_amount);
        return { balance, transfered };
    } else
        return { undefined, undefined };
};

var validateNumber= (amount) =>{
    if(!Number.isNaN(amount) && +amount>0 && +amount < Number.MAX_SAFE_INTEGER){
        return true;
    }
        return false;
}
// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
